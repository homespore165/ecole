# TP1

## Objectif

L'application permet de gérer les notes d'un groupe d'élèves et de créer des statiques.
Ce travail met en pratique principalement la gestion des tableaux 2D.

## Présentation

![](TP1/MainWindow.png)

Au lancement le fichier "notes.txt" est chargé (fichier fourni avec l'énoncé) et le premier élément est sélectionné.

* La barre de titre contient votre nom d'étudiant et votre DA.
* Le TableView notes est en mono sélection et non éditable.
* Les statistiques ne sont nécessairement pas une table view et sont non éditables.

Lors de la sélection d'un élément dans le TableView des notes, les champs TextField de la deuxième colonne sont garnis avec les données correspondantes de la ligne
sélectionnée. La ligne peut être sélectionnée avec la souris ou avec les flèches haut et bas.

### Manipulation des données

* Le bouton "Ajouter" ajoute une ligne au tableau des notes et sélectionne la ligne ajoutée. Si le DA existe déjà affiche un message d'erreur. Les TextField des notes fournissent des données valides (chiffres seulement et notes entre 0 et 100).
* Le bouton "Modifier" modifie les éléments de la ligne sélectionnée.
* Le bouton "Supprimer" supprime la ligne sélectionnée et vide les TextField.

Si la TableView est vide affiche un message d'erreur.
Pour tout changement du TableView notes, les statistiques sont mises à jour (moyenne sur deux décimales).

Le bouton "Quitter" quitte en offrant la possibilité de sauvegarder.

## Sections dans le code
Regrouper et identifier les sections dans votre code.

// Listener

// Initialisateurs

// Généralisateur

// ...


Lorsqu'un bloc de codes se répète en faire une méthode, par exemple pour mettre à jour les statistiques.

* Plus le code est découpé plus il est facile à maintenir.
* Regrouper aussi les méthodes qui vont ensembles.


## Méthodes de la Classe Utilitaires Utils.java

### moyenneEval 

Retourne la moyenne d'une colonne d'un tableau 2D d'entiers*

### minEval

Retourne la valeur minimum d'une colonne d'un tableau 2D d'entiers

### maxEval

Retourne la valeur maximum d'une colonne d'un tableau 2D d'entiers.


### fouilleDichoCol 

Fouille par indirection un tableau 2D d'entiers pour trouver un entier correspondant à la colonne
spécifiée. Si l’entier est trouvé retourne l’indice de la première occurence, sinon retourne -1.

### isPresentCol 

* Retourne si une valeur de type entier est présente ou non dans une colonne spécifiée d’un tableau 2D
d’entiers. 
* Tri par indirection sur la colonne (quickSort) et enfin effectue la fouille dichotomique sur la colonne
(fouilleDichoCol).

## IMPORTANT

* On considère que le tableau reçu n'est pas vide.
* Vous devez réaliser les tris par la façon quickSort et la fouille par la fouille dichotomique.
* Le tri est réalisé par indirection sur un tableau d'index.

## Tests unitaires UtilsTest.java

Veuillez tester les méthodes suivantes: moyenneEval, minEval, maxEval et isPresentCo

## Correction

Ce travail est à réaliser seul et à remettre pour le 30 mars

|Catégorie | Note |
|---|---|
|Fonctionnement du programme| 60 |
|&ensp;&ensp;Lecture / Écriture fichier| 10
|&ensp;&ensp;Ajout / Modification / Suppression TableView | 30
|&ensp;&ensp;Stats | 10
|&ensp;&ensp;Méthodes Utilitaires | 10 |
| Test unitaires | 10 | 
| Découpage du code en méthodes & identificateurs | 10 | 
| Commentaires & lisibilité du code | 10 | 
| **Total**  | **90** |

Il vaut pour 20% de la session.

Remise: 31 mars 2023.

## Fichiers donnés

[Exemple fichier notes.txt](TP1/notes.txt)

https://youtu.be/rtbUPdRP6UY