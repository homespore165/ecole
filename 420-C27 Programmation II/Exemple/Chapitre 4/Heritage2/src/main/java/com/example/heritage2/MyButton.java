package com.example.heritage2;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.scene.control.Button;

public class MyButton extends Button {

    public MyButton() {
        this.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                onClick(event);
            }
        });

        this.setText("Patate");
    }

    public void onClick(ActionEvent e){
        this.setText("Pressed");
    }
}
