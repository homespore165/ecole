package main.java;

import org.openjdk.jmh.annotations.Benchmark;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

public class ArrayVSArrayList {


    public static int[] addItemToArray(int[] array, int newItem){
        int[] backup = array;
        array = new int[backup.length+1];

        for (int i = 0; i < backup.length; i++ ) {
            array[i] = backup[i];
        }

        array[backup.length] = newItem;
        return array;
    }

    public static void addItemToArrayList(ArrayList<Integer> array, int newItem){
        array.add(newItem);
    }


}
