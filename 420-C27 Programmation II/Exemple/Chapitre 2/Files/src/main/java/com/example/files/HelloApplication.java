package com.example.files;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.*;

public class HelloApplication extends Application {
    @Override
    public void start(Stage stage) throws IOException {
        VBox vbox = new VBox();
        HBox hbox = new HBox();

        TextArea txaContent = new TextArea();
        Button btnSaveContent = new Button("Save");
        Button btnLoadContent = new Button("Load");

        btnLoadContent.setOnAction(event -> {
            try {
                txaContent.setText("");

                FileReader fileReader = new FileReader("abc.txt");
                BufferedReader buffReader = new BufferedReader(fileReader);

                StringBuilder sb = new StringBuilder();
                String line = buffReader.readLine();

                while (line != null) {
                    sb.append(line);
                    line = buffReader.readLine();
                }

                txaContent.setText(sb.toString());

                buffReader.close();
                fileReader.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnSaveContent.setOnAction(event -> {
            try {
                FileWriter fileWriter = new FileWriter("abc.txt");
                BufferedWriter buffWriter = new BufferedWriter(fileWriter);

                buffWriter.write(txaContent.getText());

                buffWriter.close();
                fileWriter.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        });


        hbox.getChildren().addAll(btnSaveContent, btnLoadContent);

        vbox.getChildren().addAll(hbox, txaContent);




        Scene scene = new Scene(vbox, 320, 240);
        stage.setTitle("Hello!");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}