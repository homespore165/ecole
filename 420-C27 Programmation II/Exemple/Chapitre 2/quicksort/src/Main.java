import java.util.Random;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        /*
        int [] tab1 = new int[] {19, 23, 34, 45, 31, 22, 10, 13, 25, 37};

        quicksort(tab1, 0, tab1.length-1);

        for(int i : tab1)  {
            System.out.println(i);
        }
        */


        int [] tab2 = new int[214748646];
        Random r = new Random();

        for (int i=0; i<tab2.length; i++)
            tab2[i] = r.nextInt(1000000000);

        quicksort(tab2, 0, tab2.length-1);

        for (int i=0; i<tab2.length; i++)
            System.out.println(tab2[i]);
    }

    public static void quicksort( int [] tab, int lowIndex, int highIndex) {

        if (lowIndex >= highIndex) {
            return;
        }

        /* Randomizons le pivot
        int pivot = tab[highIndex];
        * */

        int pivotIndex = new Random().nextInt(highIndex-lowIndex) + lowIndex;
        int pivot = tab[pivotIndex];
        swap(tab, pivotIndex, highIndex);

        int leftPointer = partitioning(tab, lowIndex, highIndex, pivot);
        quicksort(tab, lowIndex, leftPointer - 1 );
        quicksort(tab, leftPointer+1, highIndex);
    }

    public static void swap (int [] tab, int a, int b) {
        int temp = tab[a];
        tab[a] = tab[b];
        tab[b] = temp;
    }

    public static int partitioning (int [] tab, int lowIndex, int highIndex, int pivot) {
        int leftPointer = lowIndex;
        int rightPointer = highIndex;

        while (leftPointer < rightPointer) {
            while (tab[leftPointer] <= pivot && leftPointer < rightPointer) {
                leftPointer++;
            }
            while (tab[rightPointer] >= pivot && leftPointer <  rightPointer) {
                rightPointer--;
            }

            swap(tab, leftPointer, rightPointer);
        }

        swap(tab, leftPointer, highIndex);
        return leftPointer;
    }
}