package com.example.drag_drop;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;

public class HelloApplication extends Application {
    @Override
    public void start(Stage stage) throws IOException {
        Label label = new Label("this is AnchorPane example");
        BorderPane bp = new BorderPane(label);


        Text txtLeft = new Text("Bonjour");
        Text txtRight = new Text("Destination");

        // Appearance
        BorderPane.setAlignment(txtLeft, Pos.CENTER);
        BorderPane.setAlignment(txtRight, Pos.CENTER);
        BorderPane.setMargin(txtLeft, new Insets(12,30,12,30));
        BorderPane.setMargin(txtRight, new Insets(12,30,12,30));

        // Drag start
        txtLeft.setOnDragDetected(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent event) {
                /* drag was detected, start a drag-and-drop gesture*/
                /* allow any transfer mode */
                Dragboard db = txtLeft.startDragAndDrop(TransferMode.MOVE);

                /* Put a string on a dragboard */
                ClipboardContent content = new ClipboardContent();
                content.putString(txtLeft.getText());
                db.setContent(content);

                event.consume();
            }
        });

        // Drag on Destination (peut être accepté ou non)
        txtRight.setOnDragOver(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                /* data is dragged over the target */
                /* accept it only if it is not dragged from the same node
                 * and if it has a string data */
                if (event.getGestureSource() != txtRight &&
                        event.getDragboard().hasString()) {
                    /* allow for both copying and moving, whatever user chooses */
                    event.acceptTransferModes(TransferMode.MOVE);
                }

                event.consume();
            }
        });


        // On approuve le déplacement en indiquant le vert
        txtRight.setOnDragEntered(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                /* the drag-and-drop gesture entered the target */
                /* show to the user that it is an actual gesture target */
                if (event.getGestureSource() != txtRight &&
                        event.getDragboard().hasString()) {
                    txtRight.setFill(Color.GREEN);
                }

                event.consume();
            }
        });

        // Si l'utilisateur déplace vers un autre emplacement, le texte revient noir.
        txtRight.setOnDragExited(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                /* mouse moved away, remove the graphical cues */
                txtRight.setFill(Color.BLACK);

                event.consume();
            }
        });

        // Un coup que la souris est relâchée, que fait-on?
        txtRight.setOnDragDropped(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                /* data dropped */
                /* if there is a string data on dragboard, read it and use it */
                Dragboard db = event.getDragboard();
                boolean success = false;
                if (db.hasString()) {
                    txtRight.setText(db.getString());
                    success = true;
                }
                /* let the source know whether the string was successfully
                 * transferred and used */
                event.setDropCompleted(success);

                event.consume();
            }
        });

        txtLeft.setOnDragDone(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                /* the drag and drop gesture ended */
                /* if the data was successfully moved, clear it */
                if (event.getTransferMode() == TransferMode.MOVE) {
                    txtLeft.setText("");
                }
                event.consume();
            }
        });


        bp.setLeft(txtLeft);
        bp.setRight(txtRight);

        Scene scene = new Scene(bp, 720, 480);
        stage.setTitle("Hello!");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}