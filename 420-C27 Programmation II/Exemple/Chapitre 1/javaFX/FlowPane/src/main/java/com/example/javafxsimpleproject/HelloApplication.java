package com.example.javafxsimpleproject;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

public class HelloApplication extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        Button button1 = new Button("Button 1");
        Button button2 = new Button("Button 2");
        Button button3 = new Button("Button 3");

        FlowPane flowPane = new FlowPane();

        flowPane.getChildren().add(button1);
        flowPane.getChildren().add(button2);
        flowPane.getChildren().add(button3);

        Scene scene = new Scene(flowPane, 300, 250);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}