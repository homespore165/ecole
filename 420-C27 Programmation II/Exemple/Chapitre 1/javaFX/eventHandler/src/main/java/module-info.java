module com.example.eventhandler {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.example.eventhandler to javafx.fxml;
    exports com.example.eventhandler;
}