import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


class UtilsTest {

    @Test
    void somme() {
        assertEquals(5, Utils.somme(4, 1));
        assertEquals(-5, Utils.somme(-10,5));

        assertEquals(0, Utils.somme(Integer.MAX_VALUE, 1));
    }

    @org.junit.jupiter.api.Test
    void division() {
        //oracle 1
        assertEquals(2.5, Utils.division(5, 2));

        //oracle 2 division par zéro
        assertEquals(0, Utils.division(5, 0));

    }
}