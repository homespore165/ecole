# Lambda

Ajouté en java 8.

Les expressions lambda sont des expressions qui peuvent avoir des paramètres et qui retournent des valeurs, elles sont similaires à des méthodes mais leur distinction principale est qu'elles peuvent être implémentées directement dans des méthodes.

## Syntaxe

```java
parameter -> expression

(parameter1, parameter2) -> expression
```

Les expressions sont limitées, elles doivent assurément retournées une valeur, ne peuvent contenir de variables, d'assignations ou de tests tel que ifs ou for.

Si l'expression doivent être plus longue, on peut utiliser cette syntaxe:

```java
(parameter1, parameter2) -> { code }
```

## Exemple 

```java

    ArrayList<Integer> numbers = new ArrayList<Integer>();
    numbers.add(5);
    numbers.add(9);
    numbers.add(8);
    numbers.add(1);
    numbers.forEach( (n) -> { System.out.println(n*n); } );
```

## Supplier
Le Supplier est une interface qui représente une fonction qui offre une valeur de n'importe quel type. C'est un peu une factory (un fabricant d'objet selon la POO).

Voici un exemple:
```java
Supplier<Double> randomValue = () -> {
    Random r = new Random();
    return r.nextDouble(50.0);
};

// Print the random value using get()
System.out.println(randomValue.get());
System.out.println(randomValue.get());
System.out.println(randomValue.get());
```

Cette implementation retourne un objet de type Integer avec une valeur entre 0 et 1000.

## Consumer

L'interface fonctionnelle de Consumer est une fonction qui consomme une valeur sans rien retourner. Une idée serait d'afficher une donnée, d'écrire dans un fichier ou d'envoyer la donnée par réseau. Voici un exemple d'implémentation:

```java
import java.util.ArrayList;
import java.util.function.Consumer;

ArrayList<Integer> numbers = new ArrayList<Integer>();
numbers.add(5);
numbers.add(9);
numbers.add(8);
numbers.add(1);

Consumer<Integer> method = (n) -> { System.out.println(n); };
numbers.forEach( method );
```

Cette implémentation imprime à l'écran la valeur passée en paramètre à System.out.


# Interface fonctionnelle ( Functional Interface )

Une interface contenant qu'une seule méthode abstraite est nommée interface fonctionnelle:

```java

@FunctionalInterface
interface Square {
    int calculate(int x);
}
 
class Test {
    public static void main(String args[])
    {
        int a = 5;
 
        // lambda expression 
        Square s = (int x) -> x * x;
 
        // parameter passed and return type must be
        // same as defined in the prototype
        int ans = s.calculate(a);
        System.out.println(ans);
    }
}

ou

interface StringFunction {
  String run(String str);
}

public class Main {
  public static void main(String[] args) {
    StringFunction exclaim = (s) -> s + "!";
    StringFunction ask = (s) -> s + "?";
    printFormatted("Hello", exclaim);
    printFormatted("Hello", ask);
  }
  public static void printFormatted(String str, StringFunction format) {
    String result = format.run(str);
    System.out.println(result);
  }
}
```

# Built-in Functional Interfaces

Depuis Java 1.8, plusieurs interfaces ont été converties en interface fonctionnelle. Elle sont annotées avec @FunctionalInterface. En voici quelques unes:

## ActionListener –> actionPerformed() ou EventListener -> onEvent()

```java
@FunctionalInterface
public interface EventHandler<T extends Event> extends EventListener
```

On utilise une implémentation lambda afin de remplacer le new EventHandler(...), plus simple à lire et à maintenir.


```java
import javafx.application.*;
import javafx.event.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.*;

public class LambdaWithJavaFxTest extends Application {
   public static void main(String[] args) {
      Application.launch(args);
   }
   @Override
   public void start(Stage stage) throws Exception {
      BorderPane root = new BorderPane();
      ToggleButton button = new ToggleButton("Click");

      final StringProperty btnText = button.textProperty();
      button.setOnAction((event) -> {    // lambda expression
         ToggleButton source = (ToggleButton) event.getSource();
         if(source.isSelected()) {
            btnText.set("Clicked!");
         } else {
            btnText.set("Click!");
         }
      });
      // ou
      button.setOnAction((event) -> button1Click(event));

      root.setCenter(button);  
      Scene scene = new Scene(root);
      stage.setScene(scene);
      stage.setWidth(300);
      stage.setHeight(250);
      stage.show();
   }

   public void button1Click(EventHandler e) {
      clickCount++;
      if(clickCount == 1)
         button.setText("Clicked!!!");
      else
         button.setText("Clicked " + clickCount + " times!!!");
   }
}
```

## L'interface Comparator –> compare().

```java
    public static class Employee {
        int id;
        String name;
        double salary;
        public Employee(int id, String name, double salary) {
            super();
            this.id = id;
            this.name = name;
            this.salary = salary;
        }
    }

    public static void compare() {
        List<Employee> list = new ArrayList<Employee>();

        // Adding employees
        list.add(new Employee(115, "Charles", 75000.00));
        list.add(new Employee(125, "Sylvain",50000.00));
        list.add(new Employee(135, "Michel", 60000.00));

        System.out.println("Triage de la liste selon le salaire");

        //list.sort(new Comparator<Employee>() {
        //    @Override
        //    public int compare(Employee e1, Employee e2) {
        //        return (int) Math.round(e1.salary - e2.salary);
        //    }
        //});

        // lambda
        list.sort((e1, e2) -> {
            return (int) Math.round(e1.salary - e2.salary);
        });

        for(Employee e : list) {
            System.out.println(e.id + " " + e.name + " " + e.salary);
        }
    }
}

Output 
Sorting the employee list based on the name
115 Charles 75000.0
135 Michel 60000.0
125 Sylvain 50000.0
```


## L'interface Function

L'interface Function de Java est une des functional interfaces les plus centrales de Java. Elle représente l'exécution d'une méthode prenant un seul paramètre et retournant une seule value. Voici sa signature:

```java
public interface Function<T,R> {
    public <R> apply(T parameter);
}
```

Elle contient en réalité un peu plus de méthodes, mais puisqu'une méthode par défaut est définie on n'a pas vraiment besoin de s'en soucier. La seule méthode à implémenter est la fonctionnalité apply.

Voici un exemple:

```java
public class AddThree implements Function<Long, Long> {

    @Override
    public Long apply(Long aLong) {
        return aLong + 3;
    }
}
```

Cette fonction implémente l'utilisation de la méthode apply dans une classe implémentant l'interface Fonction.

Voici l'utilisation de cette classe:

```java
  Function<Long, Long> adder = new AddThree();
  Long result = adder.apply((long) 4);
  System.out.println("result = " + result);
```

* On crée l'instance AddThree et on lui assignes une variable locale, i-e la fonction adder.
* On appelle sur le adder la méthode apply de l'instance AddThree();
* On affiche le résultat (7)

On aurait pu s'économiser des lignes de code en utilisant une méthode lambda:

```java
Function<Long, Long> adder = (value) -> value + 3;
Long resultLambda = adder.apply((long) 4);
System.out.println("resultLambda = " + resultLambda);
```

Comme vous pouvez le constater, c'est beaucoup plus simple, graçe à la méthode lambda, tout est inline. On n'a pas eu à déclarer une nouvelle classe, vu que c'est utile uniquement à cet instant et pour ce bout de code là, pas besoin de créer une classe pour ça.

## L'interface Predicate

Représente une fonction acceptant une seule valeur comme paramètre et retourne vrai ou faux. Il s'agit en gros d'un test sur une condition:

```java
public interface Predicate<T> {
    boolean test(T t);
}
```

Comme les fonctions, il y a plus de méthodes que la méthode tests, mais il s'agit de méthode statiques ou par défaut que vous n'avez pas à implémenter.

Vous pouvez l'implémenter comme avec une classe:

```java
public class CheckForNull implements Predicate {
    @Override
    public boolean test(Object o) {
        return o != null;
    }
}
```

Vous pouvez également l'implémenter en utilisant les lambdas:

```java
    ArrayList<Integer> numbers = new ArrayList<>();
    numbers.add(5);
    numbers.add(9);
    numbers.add(8);
    numbers.add(1);
    Predicate<Integer> predicate = (value) -> value > 5;
```

Méthode possible sur l'objet ArrayList avec les prédicats
* removeIf: retire de la liste les éléments pour lesquels le prédicat est vrai.
* allMatch: retourne vrai si tous les prédicats sont vrais
* stream: retourne un élément à la fois provenant d'une collection.

```java
public class Customer {
    private String name;
    private int points;  
}

Customer john = new Customer("John P.", 15);
Customer sarah = new Customer("Sarah M.", 200);
Customer charles = new Customer("Charles B.", 150);
Customer mary = new Customer("Mary T.", 1);

List<Customer> customersWithMoreThan100Points = customers
  .stream()
  .filter(c -> c.getPoints() > 100)
  .toList();

customersWithMoreThan100Points.forEach((c)-> System.out.println(c));
```