# Interface

Définition : 

* définit un comportement qui doit être implémenté par une classe: ensemble de méthodes (juste signatures) que doit avoir une classe pour respecter l'inerface.
* un interface n’implémente aucun objet, aucune méthode. Il indique seulement son modèle.
* on dit qu’un interface est un contrat qui doit être respecté par les développeurs.
* c'est comme une classe uniquement abstraite qui impose une structure aux classes.

```java
// interface
interface Animal {
  public void animalSound();
  public void sleep(); 
}

class Pig implements Animal {
  public void animalSound() {    
    System.out.println("The pig says: groin groin");
  }
  public void sleep() {    
    System.out.println("zzz");
  }
}

class Main {
  public static void main(String[] args) {
    Pig myPig = new Pig();
    myPig.animalSound();
    myPig.sleep();
  }
}
```

### Notes sur les interfaces

* Ne peut être utiliser pour créer des objets
* Les méthodes d'interfaces n'ont pas de corps, ils sont définis par la classe implémentant l'interface
* Vous ne pouvez implémenter qu'une partie des méthodes, vous devez implémenter TOUTES les méthodes pour correspondre à l'itnerface.
* Les méthodes des interfaces sont abstraites et publiques par défaut.
* Il n'y a pas de variables dans une interface, les propriétés ainsi définies sont publiques, statiques et finales.
* Une interface ne peut contenir un constructeur (puisque l'interface ne peut créer des objets) 

### Quand utiliser une interface ?

* Java ne supporte pas les héritages multiples "multiple inheritance". Par contre, on peut utiliser les interfaces car les classes peuvent implémenter plusieurs interfaces en les séparant avec des virgules.

## Interface default

Java 8 a introduit les Default Method (ou defender methods) à l'intérieur des interfaces. Cela permet d'ajouter de nouvelles méthodes à une interface existante sans briser les implémentations actuelles. Cela permet d'ajouter une méthode qui servira d'implémentation dans une situation où une classe concrète ne répond pas à un cas d'utilisation d'une méthode d'interface.

```java
public interface newInterface {
    public void existingMethod();
    default public void newDefaultMethod() {
        System.out.println("New default method"
              " is added in interface");
    }
}
```

# Collection Interface

![](images/Collection.png)

### Interface List 

modéliser une collection ordonnée qui accepte les doublons.

### Interface Set 

modéliser une collection non ordonnée qui n'accepte pas les doublons.

### Interface Map 

modéliser une collection sous la forme clé/valeur (key/value), non ordonnée qui n'accepte pas les doublons.

## Méthodes de l'interface Collection

**boolean add(Element e)** 

ajoute un élément à la collection

**boolean addAll(Collection c)**

ajoute tous les éléments d'une collection à la collection

**boolean remove(Object o)**

supprime un élément de la collection s'il est présent

**boolean removeAll(Collection c)**

supprime tous les éléments d'une collection s'ils sont présents 

**boolean retainAll(Collection c)**

conserve que les éléments communs d'une collection les autres sont supprimés. Le boolean retourné indique si la collection a été modifiée.

**boolean contains(Object o)**

indique si l'objet est présent dans la collection

**boolean containsAll(Collection o)**

indique si les éléments d'une collection sont présents dans la collection

**void clear()**

supprime tous les éléments de la collection

**boolean isEmpty()** 

indique si la collection est vide

**int size()**

retourne le nombre d'éléments de la collection

**Object[] toArray()**

retourne un tableau des éléments de la collection
