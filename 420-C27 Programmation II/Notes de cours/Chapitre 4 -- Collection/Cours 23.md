# Enum

Dernier cours... probablement... Bravo! :D

Un Enum est un type spécial nous permettant d'avoir accès à un nombre de constantes, plutôt que d'utiliser les Entiers pour représenter un ensemble de choix.

Un exemple commun est d'utiliser les valeurs NORTH, SOUTH, EAST, WEST en un ENUM.

Puisque ce sont des ENUMS, nous tappons les noms en utilisant des majuscules.

```java
public enum Day { 
    SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY }
```

Vous devriez utiliser un ENUM lorsque vous utilisez un ensemble de constante. Ceci inclut également la liste des planètes de notre système solaire ou toute données connues à la compilation: choix d'un menu, paramètres en ligne de commande. 

```java
public class EnumTest {
    Day day;
    
    public EnumTest(Day day) {
        this.day = day;
    }
    
    public void tellItLikeItIs() {
        switch (day) {
            case MONDAY:
                System.out.println("Mondays are bad.");
                break;
                    
            case FRIDAY:
                System.out.println("Fridays are better.");
                break;
                         
            case SATURDAY: case SUNDAY:
                System.out.println("Weekends are best.");
                break;
                        
            default:
                System.out.println("Midweek days are so-so.");
                break;
        }
    }
    
    public static void main(String[] args) {
        EnumTest firstDay = new EnumTest(Day.MONDAY);
        firstDay.tellItLikeItIs();
        EnumTest thirdDay = new EnumTest(Day.WEDNESDAY);
        thirdDay.tellItLikeItIs();
        EnumTest fifthDay = new EnumTest(Day.FRIDAY);
        fifthDay.tellItLikeItIs();
        EnumTest sixthDay = new EnumTest(Day.SATURDAY);
        sixthDay.tellItLikeItIs();
        EnumTest seventhDay = new EnumTest(Day.SUNDAY);
        seventhDay.tellItLikeItIs();
    }
}

The output is:

Mondays are bad.
Midweek days are so-so.
Fridays are better.
Weekends are best.
Weekends are best.
```

## Classes vs Enums 

Les Enums peuvent avoir des méthodes et des propriétés comme les classes, ils doivent juste être final:

* Le constructeur doit être privé à l'intérieur.
* Le compilateur ajoute une méthode values qui permet de looper sur chacun des éléments.
* On ne peut ajouter d'interface à un enum.


## Comment sont stockées les données dans un Enum traditionnel ?

Généralement par un entier commençant par 0, jusqu'au nombre d'Élément dans le Enum

Afin d'optimiser nos Enums, nous pourrions utiliser EnumSet (HashSet) et EnumMap (HashMap) deux classes abstraites permettant de gérer des enums en bitwise.

# EnumSet

C'est une classe abstraite, il y a deux classes qui l'implémente:

* RegularEnumSet: utilise un long pour représenter chacun des objets à l'intérieur de l'EnumSet. Chaque bit représente un élément, comme le long a 64 bits, il peut stocker jusqu'à 64 éléments.

* JumboSet: utilise un array pour stocker les éléments permettant alors plus que 64 éléments.

```java

import java.util.EnumSet; 
enum example  
{ 
    one, two, three, four, five 
}; 
public class main 
{ 
    public static void main(String[] args)  
    { 
        // Creating a set 
        EnumSet<example> set1, set2, set3, set4; 
  
        // Adding elements 
        set1 = EnumSet.of(example.four, example.three,  
                          example.two, example.one); 
        set2 = EnumSet.complementOf(set1); 
        set3 = EnumSet.allOf(example.class); 
        set4 = EnumSet.range(example.one, example.three); 
        System.out.println("Set 1: " + set1); 
        System.out.println("Set 2: " + set2); 
        System.out.println("Set 3: " + set3); 
        System.out.println("Set 4: " + set4); 
    } 
} 
```

## EnumMap

EnumMap est un mix entre un Enum et un HashMap, mais l'avantage c'est qu'il est hautement performant:

* Les clés sont un Enum. 
* Ne permet pas les null
* Ordonné
* Représenté par des Tableaux, donc très compact et rapide.

```java

import java.util.EnumMap;
 
enum Days {
    MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY
}
 
public class EnumMapExample {
    public static void main(String[] args) {
        EnumMap<Days, String> schedule = new EnumMap<>(Days.class);
         
        // Adding elements to the EnumMap
        schedule.put(Days.MONDAY, "Work");
        schedule.put(Days.TUESDAY, "Work");
        schedule.put(Days.WEDNESDAY, "Study");
        schedule.put(Days.THURSDAY, "Study");
        schedule.put(Days.FRIDAY, "Relax");
         
        // Getting elements from the EnumMap
        System.out.println(schedule.get(Days.MONDAY)); // Output: Work
        System.out.println(schedule.get(Days.FRIDAY)); // Output: Relax
    }
}
```

