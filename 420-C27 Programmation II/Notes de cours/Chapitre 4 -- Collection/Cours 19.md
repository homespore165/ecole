# Héritage

L'héritage est une façon de créer de nouvelles classes basées sur des classes existantes. Il est possible ainsi de réutiliser du code déjà écrit.

Chaque classe crée à partie d'une classe existante se nomme classe enfant. Chaque classe existante est appelée classe parent.

Voici les critères pour la situation:

* Toutes les données et les méthodes de la classe parent sont également disponibles pour la classe enfant.
* Les membres privés de la classe parent ne peuvent être accédés par la classe enfant.
* La classe enfant peut accéder au membres publics de la classe parent.

L'écriture se fait ainsi: 

class enfant extends parent()

```java
class Animal {

  public void animalSound() {
    System.out.println("Les animaux font un son");
  }
}

class Pig extends Animal {

  public void animalSound() {
    System.out.println("Le cochon fait: groin groin");
  }
}

class Dog extends Animal {

  public void animalSound() {
    System.out.println("Le chien fait: wouf wouf");
  }
}

class Main {
  public static void main(String[] args) {
    Animal myAnimal = new Animal();  
    Animal myPig = new Pig();  
    Animal myDog = new Dog();  
    myAnimal.animalSound();
    myPig.animalSound();
    myDog.animalSound();
  }
}
```

## @Override

Permet de spécifier une nouvelle méthode dans une classe enfant une méthode de même signature qui remplacera celle du parent. Il demeure possible d'appeler celle du parent en utilisant le super. Ceci conserve l'idée de polymorphisme que permet l'orienté-objet, c'est à dire, avoir plusieurs méthodes ayant le même nom dans des classes différentes.

```java
// A Java program to illustrate Dynamic Method
// Dispatch using hierarchical inheritance
class A
{
    void m1()
    {
        System.out.println("Inside A's m1 method");
    }
}
  
class B extends A
{
    // overriding m1()
    void m1()
    {
        System.out.println("Inside B's m1 method");
    }
}
  
class C extends A
{
    // overriding m1()
    void m1()
    {
        System.out.println("Inside C's m1 method");
    }
}
  
// Driver class
class Dispatch
{
    public static void main(String args[])
    {
        // object of type A
        A a = new A();
  
        // object of type B
        B b = new B();
  
        // object of type C
        C c = new C();
  
        // obtain a reference of type A
        A ref;
          
        // ref refers to an A object
        ref = a;
  
        // calling A's version of m1()
        ref.m1();
  
        // now ref refers to a B object
        ref = b;
  
        // calling B's version of m1()
        ref.m1();
  
        // now ref refers to a C object
        ref = c;
  
        // calling C's version of m1()
        ref.m1();
    }
}

Sortie: 
Inside A's m1 method
Inside B's m1 method
Inside C's m1 method
```


## super propriété

super permet d'appeler la propriété ou la variable d'un parent

```java
class Animal{  
  String color="blanc";  
}  

class Dog extends Animal{  
  String color="noir";  
  void printColor(){  
    System.out.println(color); // affiche la couleur de la classe Dog 
    System.out.println(super.color); // affiche la couleur de la classe Animal
  }  
}

class Super1{  
  public static void main(String args[]){  
    Dog d=new Dog();  
    d.printColor();  
    // affiche noir et blanc
  }
}  
```


## super méthode

La méthode super permet a une méthode enfant d'appelée la méthode du parent:


```java
class Animal{  
  void eat(){System.out.println("mange...");}  
}  

class Dog extends Animal{  
  void eat(){
    System.out.println("mange moulée...");
  }  
  
  void bark(){
    System.out.println("jappe...");
  }    
  
  void work(){  
    super.eat();  
    bark();  
  }  
} 

class Super2{  
  public static void main(String args[]){  
    Dog d=new Dog();  
    d.work();  
  }
}  
```

## super constructeur

this permet de spécifier la classe en cours.

```java
class Person{  
  int id;  
  String name;  
  
  Person(int id, String name){  
    this.id=id;  
    this.name=name;  
  }  
}

class Emp extends Person{  
  float salary;  
  
  Emp(int id, String name, float salary){  
    super(id,name);//reusing parent constructor  
    this.salary=salary;  
  }  
  
  void display(){
    System.out.println(id+" "+name+" "+salary);
  }  
}  

class Super5{  
  public static void main(String[] args){  
    Emp e1=new Emp(1,"Michel", 45000);  
    e1.display();  
  }
}  
```

# Abstract

Une classe abstraite ne peut pas être implémentée. Dans l'exemple précédent si Animal était déclarée abstract, elle ne pourrait pas être implantée à l'appel suivant:


```java
abstract class Animal {
  public void animalSound() {
    System.out.println("Les animaux font un son");
  }
}
... 

Animal myAnimal = new Animal();
```

![](images/abstract.png)

Une méthode abstraite peut être définie dans une classe abstraite, elle n'aura aucun corps dans la classe abstraite, seulement sa déclaration. Elle forcera que les classes enfants doivent avoir définir la méthode:

```java
abstract class Animal {
  public abstract void animalSound();
}
... 

Animal myAnimal = new Animal();  // Create a Animal object
```


