# Interface Marker

Les interfaces Marker sont des façons d'informer le compilateur que la classe va avoir un comportement particulier lorsqu'on va l'instancier. 

Les interfaces Marker n'ont **aucune** méthode.

Les interfaces Marker se font nommés Tagging interface.

Voici un exemple de Marker personnalisé:

```java
public interface Deletable {
}

public class Entity implements Deletable {
    // implementation details
}

public class ShapeDao {

    public boolean delete(Object object) {
        if (!(object instanceof Deletable)) {
            return false;
        }

        // Efface l'objet

        return true;
    }
}
```

## Cloneable (https://docs.oracle.com/javase/7/docs/api/java/lang/Cloneable.html)

Permet à une classe d'être clonable, c'est à dire de répondre à l'appel à la méthode clone(). Les objets doivent implémenter Cloneable afin que l'on puisse appeler clone() sur eux. En gros, ça rend légal la méthode de copier chaque propriétés de la classe pour chaque instance de la classe.

Il est généralement recommandé d'overrider la méthode clone de Object afin d'obtenir un objet de la classe qu'on veut rendre cloneable.


```java
class Student { // implements Cloneable {

    // attributedis of Student class
    String name = null;
    int id = 0;

    // default constructor
    Student() {}

    // parameterized constructor
    Student(String name, int id)
    {
        this.name = name;
        this.id = id;
    }

    @Override
    protected Student clone()
            throws CloneNotSupportedException
    {
        return (Student) super.clone();
    }

    @Override
    public String toString(){
        return String.format("%s (%s)", this.name, this.id);
    }
}

public static void main(String[] args) {
    Student s1 = new Student("Ashish", 121);
    Student s2 = new Student();

    // Try to clone s1 and assign
    // the new object to s2
    try {
        s2 = s1.clone();

...
```

## Serialization (https://docs.oracle.com/javase/8/docs/api/java/io/Serializable.html)

Ce marker nous permet de transférer aisément un objet en Stream et la désérialization va permettre de faire l'inverse. L'opération complète est indépendante de la plateforme, c'est à dire, que vous pourriez prendre un objet sérialiser en Linux et le désérialiser sur Windows. 

Quand on dit qu'on sérialise un objet, on fait appel à la méthode suivante provenant d'un objet de type ObjectOutputStream:

```java
public final void writeObect (Object obj) throws IOException
```

Et quand on le désérialise, on fait appel à la méthode suivante provenant de la classe ObjectInputStream:

```java
public final Object readObject () throws IOException, ClassNotFoundException
```

Java nous offre une interface Serializable afin d'identifier les classes et les propriétés qui doivent être persistentes, i-e les données sont accessibles lors de l'exécution.

Tous les éléments de la classes doivent être identifiés comme sérialisable si on veut pouvoir sérialiser une classe. Par exemple, dans l'exemple ci-dessous, si un Student possédait une adresse n'étant pas Sérializable, Student ne pourra plus être sérialisé.

```java
class Student implements Serializable {

    // attributedis of Student class
    String name = null;
    int id = 0;

    // default constructor
    Student() {}

    // parameterized constructor
    Student(String name, int id)
    {
        this.name = name;
        this.id = id;
    }

    @Override
    public String toString(){
        return String.format("%s (%s)", this.name, this.id);
    }
}

public static void main(String[] args)  {
    Student s1 = new Student("Ashish", 121);
    FileOutputStream outputStream;

    try
    {
        outputStream = new FileOutputStream ("destination.txt");
        ObjectOutputStream out=new ObjectOutputStream(outputStream);
        out.writeObject(s1);
        out.flush();
        outputStream.close();
    } catch (IOException e) {
        System.out.println("Fichier non trouvé");
    }

    FileInputStream inputStream;
    Student s2 = new Student();

    try
    {
        inputStream = new FileInputStream ("destination.txt");
        ObjectInputStream in=new ObjectInputStream(inputStream);
        s2 = (Student) in.readObject();
        inputStream.close();
    } catch (IOException e) {
        System.out.println("Fichier non trouvé");
    } catch ( ClassNotFoundException e) {
        System.out.println("Structure du fichier non approprié");
    }

    System.out.println(s2);
}
```

### Transient

Lorsque nous notons un champs transient, la donnée n'est pas sérialisée:

```java
public class Book implements Serializable {
    private String bookName;
    private transient String description;
    private transient int copies;
    
    // getters and setters
}

```

Ainsi dans le main, les données descriptions et copies resteront à null et à 0 lorsque lu du fichier.

## Remote (https://docs.oracle.com/javase/8/docs/api/java/rmi/Remote.html)

Un objet remote est un objet stocké sur une machine et accédée par une autre machine. Pour rendre un objet remote, il faut lui assigner le flag d'interface Remote. Ainsi, le remote interface permet d'identifier les méthodes qui peuvent être exécutées par une machine virtuelle JVM non locale.

## RandomAccess (https://docs.oracle.com/javase/8/docs/api/java/util/RandomAccess.html)

Une interface de marquage qui permet d'indiquer que l'interface supporte un accès rapide (constant) d'accès aléatoire.

# Retour sur les collections

## Quel est le lien entre ArrayList, Collection, List, AbstractList et AbstractCollection?

* ArrayList, AbstractList et AbstractCollection sont des classes, elles peuvent donc être implémentées.
* Collection et List sont des Interfaces, elles contiennent des listes de méthodes devant être implémentées pour correspondre à ces interfaces. 
* AbstractList découle de AbstractList qui découle de AbstractCollection.

N'oubliez jamais que les objets en Java ne peuvent découler que d'une seule et unique classe, par contre, ils peuvent répondre à une multitude d'interface.
