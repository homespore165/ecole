# Lecture d'un fichier texte

Plusieurs classes afin de faciliter dans un fichier:

## BufferedReader

Cette façon de faire permet de lire un flu orienté caractères. Il utilise un buffer pour améliorer la lectures des caractères, tableaux et lignes. En général chaque opération de lecture occasionne un appel au flux de données, cependant, en le wrappant dans un BufferedReader, on réduit leur nombre. On suggère alors d'utiliser un BufferedReader autour de tout Reader qui effectue des reads qui pourraient être très gourmands en ressource tel que FileReader et InputStreamReader.

```java
BufferedReader in = new BufferedReader(Reader in, int size);
```

## FileReader

Il s'agit d'une classe afin de lire les fichiers.  Le constructeur assume un encodage de caractère par défaut et s'assure de la taille du buffer appropriée.

### Constructeurs

FileReader(File file)
FileReader(FileDescriptor fd)
FileReader(String fileName)

## Scanner  

Un simple scanner qui permet de lire et parser les types primitifs ou les chaînes en utilisant les expressions régulières.

## Files

Files.readAllLines(filepath, charset); // retourne une liste de String
Files.readAllBytes(Paths.get(fileName)); // retourne un tableau de Byte
Files.lines avec un foreach

### Exemple

```java
try {
    txaContent.setText("");

    FileReader fileReader = new FileReader("abc.txt");
    BufferedReader buffReader = new BufferedReader(fileReader);

    StringBuilder sb = new StringBuilder();
    String line = buffReader.readLine();

    while (line != null) {
        sb.append(line);
        line = buffReader.readLine();
    }

    txaContent.setText(sb.toString());

    buffReader.close();
    fileReader.close();

} catch (IOException e) {
    e.printStackTrace();
}
```

# Écriture d'un fichier texte

## FileWriter

FileWriter est le moyen le plus simple d'écrire un fichier en java. Il écrit directement dans un fichier et devrait être utilisé quand le nombre d'écriture est bas. Il n'utilise pas de buffer.

## BufferedWriter

BufferedWriter est similaire à FileWriter mais il utilise des buffer interne afin d'écrire des données dans un fichier.

## FileOutputStream

L'écriture de fichier autre que texte.

## Files

Une classe utilitaire que vous devriez utiliser avec parsimonie, elle a été remplacée avec des classes pour vous aider.

### Exemple

```java
FileWriter fileWriter = new FileWriter("abc.txt");
BufferedWriter buffWriter = new BufferedWriter(fileWriter);

buffWriter.write(txaContent.getText());

buffWriter.close();
fileWriter.close();
java
