# Classes et objets

## Présentation d'une classe

![](img/Classe1.png)

Une classe est la définition d'un type de données, on y définit:

* données membres
* méthodes
* constructeurs
* accesseurs (get) et mutateurs (set)
* gestion de la classe

## Portée
En programmation, la portée détermine qui peut faire appel à une classe, à une donnée membre d'une classe ou à une méthode d'une classe.

* public accessible à toutes les classes du package
* private accessible à l'intérieur de la classe

![](img/Classe2.png)

## Instanciation (création d'un objet d'une classe)

![](img/Classe3.png)

## Surcharge du constructeur

![](img/Classe4.png)

## Mot clé static

* La POO permet d'instancier un objet d'une classe. Il existe des situations où des données et des méthodes n'ont pas à être instanciées dans un objet.
* Le mot réservé "static" permet à une données d'être déclarée en mémoire sans création d'objets de sa classe.
* Le mot réservé "static" permet à une méthode d'être déclarée et utilisée sans création d'objets de sa classe.
* On accède aux données "static" directement plutôt qu'avec un objet.

![](img/Classe5.jpg)



