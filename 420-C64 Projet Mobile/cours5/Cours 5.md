# Navigation and routing

Flutter utilise un système complet afin de permettre la navigation entre les écrans. Les applications simples devraient seulement utiliser le Navigator.  Celles ayant de fonctionnalités plus avancés devraient se tourner vers le Router.

Voyons en détail les deux utilisations.

## Navigator

```dart
    body: Center(
        child: TextButton(
            child: const Text('View Details'),
            onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) {
                        return DetailScreen();
                    }),
                );
            },
        ),
    ),
```

Un simple bouton, lorsqu'on clique dessus, on empile la page DetailScreen.

Comme le Navigator garde une pile des différents objets de Route(représentant un historique), il faut lui passer un Route object. L'objet MaterialPageRoute est une sous-classe de route qui spécifie une transition appropriée pour le design Material.

Dans le second widget, DetailScreen, on a l'information suivante:

```dart
body: Center(
        child: TextButton(
          child: const Text('Pop!'),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
```

## Routes nommés (à proscrire)

Flutter ne recommande plus d'utiliser cette façon de faire même si elle permettait de faire des routes complètes et assez précises. Le comportement est toujours le même, i-e une nouvelle route est empilée sur le Navigator sans se soucier d'où est l'utilisateur.


### Limitations 

Étant donné qu'il ne permettait pas d'utiliser les boutons forwards des navigateurs. Comme flutter veut devenir un langage pour toutes les plateformes, il faut aussi qu'il respecte les navigateurs webs.

```dart
MaterialApp(
    title: 'Named Routes Demo',
    // Start the app with the "/" named route. In this case, the app starts
    // on the FirstScreen widget.
    initialRoute: '/',
    routes: {
    // When navigating to the "/" route, build the FirstScreen widget.
    '/': (context) => const FirstScreen(),
    // When navigating to the "/second" route, build the SecondScreen widget.
    '/second': (context) => const SecondScreen(),
    },
),

...

child: ElevatedButton(
    // Within the `FirstScreen` widget
    onPressed: () {
    // Navigate to the second screen using a named route.
    Navigator.pushNamed(context, '/second');
    },
    child: const Text('Launch screen'),
),
```

## Passage d'information

### Recevoir de l'information d'un widget

Un peu comme ce que nous avions vu à date avec les Future:

```dart
final result = await Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => const SelectionScreen()),
);
```

On await l'exécution du code lorsqu'on appelle une nouvelle page. Celle-ci se fait exécuter. Et au retour, on peut continuer notre traitement:

```dart
ScaffoldMessenger.of(context)
    ..removeCurrentSnackBar()
    ..showSnackBar(SnackBar(content: Text('$result')));
```

Afin de retourner les données à la "page appelante", on utilise ceci dans le pop du Navigator:

```dart
child: ElevatedButton(
    onPressed: () {
        // Close the screen and return "Yep!" as the result.
        Navigator.pop(context, 'Yep!');
    },
    child: const Text('Yep!'),
),
```

Donc c'est la chaîne 'Yep!' qui sera retournée au final que l'on a vu précédemment.

### Envoyer de l'information à un widget

En créant le widget de destination dans le NavigatorPush, il est possible de lui envoyer de l'information tel que dans le dernier exemple Parcourt d'une liste:

```dart
Navigator.push(
  context,
  MaterialPageRoute(
    builder: (context) => DetailScreen(
        todo: todos[index]
    ),
  ),
);

...

class DetailScreen extends StatelessWidget {
  // In the constructor, require a Todo.
  const DetailScreen({Key? key, required this.todo}) : super(key: key);

  // Declare a field that holds the Todo.
  final Todo todo;

  @override
  Widget build(BuildContext context) {
    // Use the Todo to create the UI.
    return Scaffold(
      appBar: AppBar(
        title: Text(todo.title),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Text(todo.description),
      ),
    );
  }
```

Le widget de destination contient une variable permettant de stocker la donnée todo qui lui est passée.

C'est d'ailleurs avec cette information qu'il bâtit son interface.

