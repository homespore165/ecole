class name {
  final String first;
  final String last;
  final String title;

  const name({
    required this.first,
    required this.last,
    required this.title,
  });

  @override
  String toString() {
    return title + ". " + first + " " + last;
  }

  factory name.fromJson(Map<String, dynamic> json) {
    return name(
      first: json['first'],
      last: json['last'],
      title: json['title'],
    );
  }
}
