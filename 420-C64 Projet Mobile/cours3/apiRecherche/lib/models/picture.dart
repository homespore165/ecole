class picture {
  final String large;
  final String medium;
  final String thumbnail;

  const picture({
    required this.large,
    required this.medium,
    required this.thumbnail,
  });

  factory picture.fromJson(Map<String, dynamic> json) {
    return picture(
        large: json['large'],
        medium: json['medium'],
        thumbnail: json['thumbnail']);
  }
}
