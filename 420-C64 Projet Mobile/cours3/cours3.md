# Null

3 principes de base:

* Non-nullable par défaut. Ceci fut choisie lors d'une recherche qui a prouver que non-null est le choix par défaut lors d'utilisation d'API.

* Incrementally adoptable. Vous pouvez choisir de migrer quand vous le souhaitez. Vous pouvez également mélanger du null-safe et du non-null-safe dans le même projet. Il y a même des outils offerts pour le détecter.

* Fully sound. L'utilisation de la protection contre le null permet l'optimisation du compilateur. Si qqch ne peut être null, il ne le sera jamais et pourra être optimisé en ce sens. L'avantage principal d'être totalement null-safe est de plus petits binaires et une plus grande vitesse d'exécution.

## Type null

Par défaut, toutes vos variables sont non-nulles. Si vous souhaitez ajouter un type nullable, vous devez utiliser ? dans sa déclaration.

```dart
void main() {
  int? a;
  a = null;
  print('a is $a.');
}
```

## Null Assertion

En utilisan le !, on détermine que l'objet en cours ne peut être nul:

```dart
// Fonction inline qui retourne -3
int? couldReturnNullButDoesnt() => -3;

void main() {
  int? couldBeNullButIsnt = 1;
  List<int?> listThatCouldHoldNulls = [2, null, 4];

  int a = couldBeNullButIsnt;
  int b = listThatCouldHoldNulls.first!; // first item in the list
  int c = couldReturnNullButDoesnt()!.abs(); // absolute value

  print('a is $a.');
  print('b is $b.');
  print('c is $c.');
}
```

## Null-aware operation

### Conditional property

```dart
int? stringLength(String? nullableString) {
  return nullableString?.length;
}
```

En utilisant le ?, on s'assure que l'objet est bien non-null avant de tenter d'accéder à sa longueur.

### Null-coalescing

```dart
// Affiche alternate si nullableString est null
print(nullableString ?? 'alternate');
print(nullableString != null ? nullableString : 'alternate');

// Avec un égal, permet l'assignation directement si la donnée à gauche est null, les deux affectent donc alternate:
nullableString ??= 'alternate';
nullableString = nullableString != null ? nullableString : 'alternate';
```

# Future

On a déjà vu ce sujet lors de l'utilisation d'un datepicker. Mais sa véritable force provient lors d'une connexion à un API comme aujourd'hui­.

En Dart, lorsqu'on doit obtenir des données de façon asynchrone (API, BD, ...), on utilise une fonction Future. 

```dart
Future<String> getValue() async {
  await Future.delayed(Duration(seconds: 3));
  return 'Bonjour!';
}

ou

Future<User> fetchUser() async {
  final response = await http
      .get(Uri.parse('https://randomuser.me/api/?gender=' + apiGender));

  if (response.statusCode == 200) {
    final json = response.body;
    final extractedData = jsonDecode(json);

    List users = extractedData["results"];
    var choosingOneUser = Random().nextInt(users.length);
    return user.fromJson(users[choosingOneUser]);
  } else {
    throw Exception('Impossible de trouver une personne');
  }
}

```

# FutureBuilder

Dans le onbuild de votre widget, on peut utiliser un FutureBuilder qui peut se mettre à jour lorsque le Fuure est complétée.

Ici par contre, à chaque fois qu'on va redessiner le builder, on va appeler geValue à chaque fois().

```dart
FutureBuilder(
future: getValue(),
// other arguments
),
```

Ce n'est pas nécessairement la meilleure technique, car à chaque fois que le widget va se faire redessiner, la méthode va être appelée.

On peut alors l'initialiser dans la méthode initState:

```dart
Future<String> _value;

@override
initState() {
  super.initState();
  _value = getValue();
}
```

## initState
initState() est une méthode appelée une seule fois, elle permet l'initialisation des objets ou de variables utilisées par le widget. Elle n'est appelée qu'une seule fois, lors de la création du widget.


Ainsi on peut tout bonnement utiliser:

```dart
FutureBuilder<String>(
  future: _value,
  builder: (
      context,
      snapshot,
      ) {
    if (snapshot.connectionState == ConnectionState.waiting) {
      return CircularProgressIndicator();
    } else if (snapshot.connectionState == ConnectionState.done) {
      if (snapshot.hasError) {
        return const Text('Error');
      } else if (snapshot.hasData) {
        return Text(
            snapshot.data,
            style: const TextStyle(color: Colors.cyan, fontSize: 36)
        );
      } else {
        return const Text('Empty data');
      }
    } else {
      return Text('State: ${snapshot.connectionState}');
    }
  },
),
```

## ConnectionState?

Un Enum qui peut avoir ses états:

* none: Non connecté à aucune foncion asynchrone.
* waiting: Associé avec une fonction asynchrone, mais elle n'a pas finie de s'exécutée.
* done: Associée avec la fin d'une fonction asynchrone. Le future a terminé.

## Snapshot

snapshot.hasData permet de vous assurer qu'il y a des données et à partir de là, on peut utiliser le data pour parcourir les données du snapshot qui nous sont retournés par le Future et les utiliser pour mettre à jour nos interfaces.
