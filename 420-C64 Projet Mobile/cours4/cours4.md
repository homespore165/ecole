# JSON

Soit le json suivant:
```json
{
  "name": "John Smith",
  "email": "john@example.com"
}
```

En utilisant la librairie dart:convert, nous pouvons traduire notre json en variable facilement accessible:

```dart
Map<String, dynamic> user = jsonDecode(jsonString);

print('Bonjour ${user['name']}!');
print('Votre lien de connexion est disponible au courriel suivant: ${user['email']}.');
```

Pour sérialiser directement entre un json et une classe, vous devez réaliser ce qui a été fait dans les exemples du cours3, i-e de sérialiser et de désérialiser directement au besoin, vos classes auront à implémenter les deux méthodes suivantes:

```dart
User.fromJson(Map<String, dynamic> json)
    : name = json['name'],
      email = json['email'];

Map<String, dynamic> toJson() => {
    'name': name,
    'email': email,
};

Map<String, dynamic> userMap = jsonDecode(jsonString);
var user = User.fromJson(userMap);

print('Bonjour ${user.name}!');
print('Votre lien de connexion est disponible au courriel suivant: ${user.email}.');
```

Pour encoder en JSON à partir de là, on peut tout bonnement utiliser 

```dart
String json = jsonEncode(user); 
```

## Sérialisable

Le paquet json_serializable disponible sur pub.dev, vous permet d'automatiser le transfert d'un vers l'autre, plus facilement (sans faire l'assignation manuelle des variables).

```dart
// Copyright (c) 2017, the Dart project authors. Please see the AUTHORS file
// for details. All rights reserved. Use of this source code is governed by a
// BSD-style license that can be found in the LICENSE file.

import 'package:json_annotation/json_annotation.dart';

part 'example.g.dart';

@JsonSerializable()
class Person {
  /// The generated code assumes these values exist in JSON.
  final String firstName, lastName;

  /// The generated code below handles if the corresponding JSON value doesn't
  /// exist or is empty.
  final DateTime? dateOfBirth;

  Person({required this.firstName, required this.lastName, this.dateOfBirth});

  /// Connect the generated [_$PersonFromJson] function to the `fromJson`
  /// factory.
  factory Person.fromJson(Map<String, dynamic> json) => _$PersonFromJson(json);

  /// Connect the generated [_$PersonToJson] function to the `toJson` method.
  Map<String, dynamic> toJson() => _$PersonToJson(this);
}
```

Lors de la compilation les méthodes suivantes sont automatiquement générées:

```dart
part of 'example.dart';

Person _$PersonFromJson(Map<String, dynamic> json) => Person(
      firstName: json['firstName'] as String,
      lastName: json['lastName'] as String,
      dateOfBirth: json['dateOfBirth'] == null
          ? null
          : DateTime.parse(json['dateOfBirth'] as String),
    );

Map<String, dynamic> _$PersonToJson(Person instance) => <String, dynamic>{
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'dateOfBirth': instance.dateOfBirth?.toIso8601String(),
    };
```

# Unit test

https://docs.flutter.dev/cookbook/testing/unit/introduction

Fichier pubspec.yaml
```
dev_dependencies:
  test: <latest_version>
```

On crée un fichier rattaché à la classe à tester:
```
counter_app/
  lib/
    counter.dart
  test/
    counter_test.dart
```

Soit la classe:

```dart
class Counter {
  int value = 0;

  void increment() => value++;

  void decrement() => value--;
}
```

Quelle serait les meilleures méthodes afin de s'assurer que notre classe fonctionne malgré toute les opérations.

```dart
// Import the test package and Counter class
import 'package:counter_app/counter.dart';
import 'package:test/test.dart';

void main() {
  test('Counter value should be incremented', () {
    final counter = Counter();

    counter.increment();

    expect(counter.value, 1);
  });
}


