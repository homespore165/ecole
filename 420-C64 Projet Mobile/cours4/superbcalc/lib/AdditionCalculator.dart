import 'package:flutter/material.dart';

class AdditionCalculator extends StatefulWidget {
  const AdditionCalculator({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<AdditionCalculator> createState() => _AdditionCalculator();
}

class _AdditionCalculator extends State<AdditionCalculator> {
  TextEditingController controllerAdd1 = TextEditingController(text: "0");
  TextEditingController controllerAdd2 = TextEditingController(text: "0");
  int sum = 0;

  void _recalculateNumber(String text) {
    var number1 = int.tryParse(controllerAdd1.text) ?? 0;
    var number2 = int.tryParse(controllerAdd2.text) ?? 0;
    setState(() {
      sum = number1 + number2;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        TextFormField(
          controller: controllerAdd1,
          key: const Key('textAdd1'),
          keyboardType: TextInputType.number,
          onChanged: _recalculateNumber,
        ),
        TextFormField(
          controller: controllerAdd2,
          key: const Key('textAdd2'),
          keyboardType: TextInputType.number,
          onChanged: _recalculateNumber,
        ),
        Text(
          '$sum',
          key: const Key('resultAdd'),
          style: Theme.of(context).textTheme.headline4,
        ),
      ],
    );
  }
}
